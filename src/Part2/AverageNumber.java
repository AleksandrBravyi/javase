package Part2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class AverageNumber {
    public static void main(String[] args) throws ParseException {
        System.out.println("Введите числа");
        Scanner scanner = new Scanner(System.in);
        String[] q = scanner.nextLine().split(",");
        System.out.println("Вы ввели " + Arrays.toString(q));

        int sum = 0;
        for (String s : q)
            sum += s.length();
        int average = sum / q.length;

        System.out.println("Среднее значение: " + average);

        int[] more = new int[1];
        int[] less = new int[1];
        for (String value : q) {
            if (average < value.length()) {
                more[more.length - 1] = Integer.parseInt(value);
                more = Arrays.copyOf(more, more.length + 1);
            } else if (average > value.length()) {
                less[less.length - 1] = Integer.parseInt(value);
                less = Arrays.copyOf(less, less.length + 1);
            }
        }

        more = Arrays.copyOf(more, more.length - 1);
        less = Arrays.copyOf(less, less.length - 1);

        System.out.println("Больше среднего значения");
        for (int i = 0; i < more.length; i++) {
            System.out.println("Число: " + more[i] + " длина: " + String.valueOf(more[i]).length());
        }

        System.out.println("Меньше среднего значения");
        for (int i = 0; i < less.length; i++) {
            System.out.println("Число: " + less[i] + " длина: " + String.valueOf(less[i]).length());
        }
        System.out.println("Name: Aleksandr Bravyi");
        Date date = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse("29.07.2021");
        System.out.println("Дата получения " + date);
        System.out.println("Дата сдачи " + new Date());
    }
}
