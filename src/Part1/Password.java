package Part1;

import java.util.Scanner;

public class Password {
    public static void main(String[] args) {
        int truePassword = 12421;
        Scanner password = new Scanner(System.in);
        System.out.println("Введите пароль");
        while (true) {
            int x = 0;
            if (password.hasNextInt()) {
                x = Integer.parseInt(password.nextLine());
            } else {
                System.out.println("Это не число, введите цифры");
                password.nextLine();
                continue;
            }
            if (x == truePassword) {
                System.out.println("Вы ввели правильный пароль");
                break;
            } else {
                System.out.println("Вы ввели не правильный пароль");
            }
        }
    }
}
