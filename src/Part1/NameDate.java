package Part1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NameDate {
    public static void main(String[] args) throws ParseException {
        System.out.println("Name: Aleksandr Bravyi");
        Date date = new SimpleDateFormat( "dd.MM.yyyy" ).parse( "28.12.2016" );
        System.out.println(new Date());
    }
}
