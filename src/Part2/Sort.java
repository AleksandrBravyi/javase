package Part2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Sort {
    public static void main(String[] args) throws ParseException {
        System.out.println("Введите числа");
        Scanner scanner = new Scanner(System.in);
        String[] numbers = scanner.nextLine().split(",");
        System.out.println("Вы ввели " + Arrays.toString(numbers));
        System.out.println(Arrays.toString(numbers));

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = numbers.length - 1; j > i; j--) {
                if (numbers[j - 1].replaceAll("-", "").length() > numbers[j].replaceAll("-", "").length()) {
                    String tmp = numbers[j - 1];
                    numbers[j - 1] = numbers[j];
                    numbers[j] = tmp;
                }
            }
        }

        System.out.println(Arrays.toString(numbers));
        System.out.println("Name: Aleksandr Bravyi");
        Date date = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse("29.07.2021");
        System.out.println("Дата получения " + date);
        System.out.println("Дата сдачи " + new Date());
    }
}
//        int[] numbers = new int[numbersStr.length];
//        for (int i = 0; i < numbersStr.length; i++) {
//            numbers[i] = Integer.parseInt(numbersStr[i]);
//        }

//        for (int i = 0; i < numbers.length - 1; i++) {
//            for (int j = numbers.length - 1; j > i; j--) {
//                if (Math.abs(numbers[j - 1]) > Math.abs(numbers[j])) {
//                    int tmp = numbers[j - 1];
//                    numbers[j - 1] = numbers[j];
//                    numbers[j] = tmp;
//                }
//            }
//        }