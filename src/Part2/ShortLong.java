package Part2;

import java.sql.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class ShortLong {
    public static void main(String[] args) throws ParseException {
        System.out.println("Введите числа");
        Scanner s = new Scanner(System.in);
        String[] q = s.nextLine().split(",");
        System.out.println("Вы ввели " + Arrays.toString(q));
        int maxSize = 0;
        int minSize = 0;
        int maxValue = 0;
        int minValue = 0;
        for (String value : q) {
            if (maxSize < value.length()) {
                maxSize = value.length();
            }

            if (maxValue < Integer.parseInt(value)) {
                maxValue = Integer.parseInt(value);
            }

            if (minSize > value.length() || minSize == 0) {
                minSize = value.length();
            }

            if (minValue > Integer.parseInt(value) || minValue == 0) {
                minValue = Integer.parseInt(value);
            }
        }
        System.out.println(minValue + " с количеством символов " + minSize);
        System.out.println(maxValue + " с количеством символов " + maxSize);
        System.out.println("Name: Aleksandr Bravyi");
        Date date = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse("29.07.2021");
        System.out.println("Дата получения " + date);
        System.out.println("Дата сдачи " + new Date());
    }
}
