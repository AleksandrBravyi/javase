package Part2;

import java.util.Arrays;
import java.util.Scanner;

public class Increase {
    public static void main(String[] args) {
        System.out.println("Введите числа");
        Scanner scanner = new Scanner(System.in);
        String[] numbers = scanner.nextLine().split(",");
        System.out.println("Вы ввели " + Arrays.toString(numbers));
        String str = "Hello";
        for (String num : numbers) {
            char tmp = 0;
            boolean flag = true;
            for (int i = 0; i < num.length(); i++) {
                if (tmp < num.charAt(i)) {
                    tmp = num.charAt(i);
                } else {
                    flag = false;
                    break;
                }
            }
            if (flag){
                str = num;
                break;
            }
        }
        System.out.println(str);
    }
}
