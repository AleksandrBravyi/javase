package Part2;

import java.util.*;

public class DifferentNumbers {
    public static void main(String[] args) {
        String[] numbers = {"415151", "12413", "31131", "31516", "41516", "31517965", "11111"};
        int[] result = new int[0];
        System.out.println(Arrays.toString(numbers));

        for (String num : numbers) {
            Set<Character> differentNumbers = new HashSet<>();
            for (int i = 0; i < num.length(); i++) {
                differentNumbers.add(num.charAt(i));
            }
            result = Arrays.copyOf(result, result.length + 1);
            result[result.length - 1] = differentNumbers.size();
        }
        System.out.println(Arrays.toString(result));
        int min = 0;
        int index = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i] < min || min == 0) {
                min = result[i];
                index = i;
            }
        }
        System.out.println("Минимальное различие цифр в числе: " + numbers[index]);
    }
}